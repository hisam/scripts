#!/usr/bin/env python3

import pandas as pd
from helpers import ppnlist2pandas, timer
import argparse
import sys

default_output = [sys.argv[0].replace(".py", "_output")]

parser = argparse.ArgumentParser(
    description="Processes a list of ppn: fetches informations from server and returns a file.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument(
    "--format",
    "-f",
    nargs=1,
    choices=["xml", "json", "csv"],
    default=["csv"],
    help="The output format.",
)
parser.add_argument(
    "--output", "-o", nargs=1, default=default_output, help="The output file."
)
parser.add_argument(
    "ppn_array", nargs="+", action="store", help="a space separated list of ppn."
)
args = parser.parse_args(sys.argv[1:])

# Trick for handling powershell not passing an array
if len(args.ppn_array) == 1:
    args.ppn_array = args.ppn_array[0].split()


@timer
def main(output, output_format, ppn_array):
    data, success = ppnlist2pandas(ppn_array)
    success_record = dict(zip(ppn_array, success))
    print(f"Successful requests =0, and failure =1:\n {success_record}\n")
    if output_format[0] == "xml":
        formatted_data = data.to_xml(
            index=False, parser="etree", encoding="utf-8")
    elif output_format[0] == "csv":
        formatted_data = data.to_csv()
    elif output_format[0] == "json":
        formatted_data = data.to_json(orient="records")

    with open(output[0] + "." + output_format[0], "w", encoding="utf-8") as file:
        file.write(formatted_data)


if __name__ == "__main__":
    main(args.output, args.format, args.ppn_array)
