#!/usr/bin/env python3

import xml.etree.ElementTree as xtree
from helpers import with_output, timer, fetch_xml_hebis
import sys
import argparse

default_output = [sys.argv[0].replace(".py", "_output")]

parser = argparse.ArgumentParser(
    description="Process a list of ppn and return a file.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "-o", "--output", nargs=1, default=default_output, help="the output file."
)
parser.add_argument(
    "ppn_array", nargs="+", action="store", help="a space separated list of ppn."
)
args = parser.parse_args(sys.argv[1:])


@timer
def main(ppn_array):
    output_data = xtree.Element("records")
    ns = {
        "srw": "http://www.loc.gov/zing/srw/",
        "marc": "http://www.loc.gov/MARC21/slim",
    }

    for idx, ppn in enumerate(ppn_array):

        @with_output(idx, len(ppn_array) - 1)
        def add_record(data):
            raw_data = fetch_xml_hebis(ppn)
            root = xtree.fromstring(raw_data)
            for records in root.findall(".//srw:recordData", ns):
                for record in records.findall(".//marc:record", ns):
                    data.append(record)

        add_record(output_data)

    tree = xtree.ElementTree(output_data)
    with open(args.output[0] + ".xml", "wb") as file:
        tree.write(file, encoding="utf-8", xml_declaration=True)


if __name__ == "__main__":
    main([str(ppn) for ppn in args.ppn_array])
