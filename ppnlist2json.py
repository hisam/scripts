#!/usr/bin/env python3

import json
import xml.etree.ElementTree as xtree
from helpers import with_output, timer, fetch_xml_hebis
import sys
import argparse

default_output = [ sys.argv[0].replace(".py", "_output") ]

parser = argparse.ArgumentParser(
    description="Process a list of ppn and return a file.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument(
    "-o", "--output", nargs=1, default=default_output, help="the output file."
)
parser.add_argument(
    "ppn_array", nargs="+", action="store", help="a space separated list of ppn."
)
args = parser.parse_args(sys.argv[1:])

# Trick for handling powershell not passing an array
if len(args.ppn_array) == 1:
    args.ppn_array = args.ppn_array[0].split()


@timer
def main(ppn_array):
    output_data = []
    ns = {
        "srw": "http://www.loc.gov/zing/srw/",
        "marc": "http://www.loc.gov/MARC21/slim",
    }

    for idx, ppn in enumerate(ppn_array):

        @with_output(idx, len(ppn_array) - 1)
        def add_record(data):
            raw_data = fetch_xml_hebis(ppn)
            root = xtree.fromstring(raw_data)
            for records in root.findall(".//srw:recordData", ns):
                record = {}
                for categories in records.findall(".//marc:datafield", ns):
                    record[categories.attrib["tag"]] = {}
                    for values in categories.findall(".//"):
                        record[categories.attrib["tag"]][
                            values.attrib["code"]
                        ] = values.text
                data.append(record)

        add_record(output_data)

    with open(args.output[0] + ".json", "w") as file:
        json.dump(output_data, file)


if __name__ == "__main__":
    main([str(ppn) for ppn in args.ppn_array])
