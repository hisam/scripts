#!/usr/bin/env python3

import sys
import time
import functools
import urllib3
from urllib import parse as urlparse
from urllib3.util import Retry
from urllib3.exceptions import MaxRetryError

import xml.etree.ElementTree as xtree
import pandas as pd


def timer(func):
    """Print the runtime of the decorated function"""

    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()  # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()  # 2
        run_time = end_time - start_time  # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value

    return wrapper_timer


def with_output(index, array_length):
    def wrap(func):
        def wrapped_f(*args, **kwargs):
            msg = "Processing element %i of %i." % (index, array_length)
            sys.stdout.write(msg + chr(8) * len(msg))
            func(*args, **kwargs)
            sys.stdout.flush()

        return wrapped_f

    return wrap


def fetch_xml_hebis(ppn, register_success=None):
    http = urllib3.PoolManager()
    cql_query = {"pica.ppn": ppn}
    cql_components = {
        "version": 1.1,
        "operation": "searchRetrieve",
        "query": urlparse.urlencode(cql_query),
        "recordSchema": "marc21",
        "maximumRecords": 10,
    }
    request = urlparse.SplitResult(
        scheme="http",
        netloc="sru.hebis.de",
        path="/sru/DB=2.1",
        query=urlparse.urlencode(cql_components),
        fragment="",
    ).geturl()
    try:
        response = http.request("GET", request, timeout=10, retries=10)
    except MaxRetryError as e:
        if register_success is not None:
            register_success.append(0)
        print("PPN {} failed due to {}".format(ppn, e.reason))
        return

    data = response.data.decode("utf-8")
    if register_success is not None:
        root = xtree.fromstring(data)
        ns = {"srw": "http://www.loc.gov/zing/srw/"}
        records_num = root.find(".//srw:record_number", ns)
        if records_num is None or records_num.text == 0:
            register_success.append(0)
        else:
            register_success.append(1)
    return data


def ppnlist2pandas(ppn_array):
    data = []
    success = []
    ns = {
        "srw": "http://www.loc.gov/zing/srw/",
        "marc": "http://www.loc.gov/MARC21/slim",
    }

    for idx, ppn in enumerate(ppn_array):

        @with_output(idx, len(ppn_array) - 1)
        def add_record(data):
            raw_data = fetch_xml_hebis(ppn, register_success=success)
            root = xtree.fromstring(raw_data)
            for records in root.findall(".//srw:recordData", ns):
                record = {}
                for categories in records.findall(".//marc:datafield", ns):
                    if categories.attrib["tag"] != "924":
                        for values in categories.findall(".//"):
                            record[
                                "MARC21_"
                                + categories.attrib["tag"]
                                + "_"
                                + values.attrib["code"]
                            ] = values.text
                    else:
                        for values in categories.findall(".//"):
                            index = "".join(
                                [
                                    "MARC21_",
                                    categories.attrib["tag"],
                                    "_",
                                    values.attrib["code"],
                                ]
                            )
                            if not record.get(index):
                                record[index] = values.text
                            else:
                                record[index] = ",".join(
                                    [record[index], values.text])

                data.append(record)

        add_record(data)
    return pd.DataFrame(data), success
