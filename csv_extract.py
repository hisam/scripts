#!/usr/bin/env python3

import csv
import argparse
import sys


parser = argparse.ArgumentParser(
    description="Extract a field out of a csv file and returns a single array."
)
parser.add_argument(
    "input",
    action="store",
    help="The input file (csv file or equivalent, tsv).",
)
parser.add_argument(
    "--column",
    "-c",
    nargs=1,
    type=int,
    default=[0],
    action="store",
    help="The column to extract. Default=0",
)
parser.add_argument(
    "--skip-header",
    default=True,
    action=argparse.BooleanOptionalAction,
    help="Skip header (first row).",
)
args = parser.parse_args(sys.argv[1:])


def main(inputfile, column, skipheader):
    output = []
    with open(inputfile, "r", newline="", encoding="utf-8") as file:
        csvreader = csv.reader(file)
        if skipheader:
            next(csvreader, None)
        for row in csvreader:
            output.append(row[column])
    print(" ".join(output))


if __name__ == "__main__":
    main(args.input, args.column[0], args.skip_header)
