#!/usr/bin/env python3

import csv
import sys
import argparse
import re

parser = argparse.ArgumentParser(
    description="Processes a csv file as produced by winibw and returns a standard csv file."
)
parser.add_argument("input", nargs=1, action="store", help="The input file.")
parser.add_argument(
    "--output",
    "-o",
    nargs=1,
    help='The output file. Default to input file minus extension + "_output.csv"',
)
args = parser.parse_args(sys.argv[1:])
if args.output is None:
    args.output = [re.sub(r"\.[a-z]*$", "_output.csv", args.input[0])]
print(args.output)


def get_csv_dialect(input_file):
    sniffer = csv.Sniffer()
    with open(input_file, "r", encoding="utf-8-sig") as file:
        sample = file.read()
    dialect = sniffer.sniff(sample)
    return dialect


def main(input_file, output_file):
    input_dialect = get_csv_dialect(input_file)
    with open(input_file, "r", newline="", encoding="utf-8-sig") as csv_input:
        reader = csv.reader(csv_input, dialect=input_dialect)
        with open(output_file, "w", newline="", encoding="utf-8") as csv_output:
            writer = csv.writer(csv_output, dialect="excel")
            for row in reader:
                writer.writerow(row)


if __name__ == "__main__":
    main(args.input[0], args.output[0])
