#!/usr/bin/env python3


def extract_structure(record):
    """Extract the structure of a given record(as dictionary).
    Return a dictionary of keys and value types."""
    output = {}
    for k, v in record.items():
        if isinstance(v, dict):
            output[k] = extract_structure(v)
        else:
            try:
                v = int(v)
            except ValueError:
                pass
            output[k] = type(v)
    return output


def meta_structure(record, struc=None, prefix="", separator=","):
    "Create a metatables: values as keys, an concatenated keys as values."
    if struc is None:
        struc = {}
    for k, v in record.items():
        if isinstance(v, dict):
            struc = meta_structure(v, struc=struc, prefix=k + separator)
        else:
            struc[v] = prefix + k
    return struc


def create_mapping(record1, record2):
    """Compare the same records in different formats, with two meta-tables,
    and for similar values map the keys in record1 to keys in record2."""
    mapping = {}
    meta_record1 = meta_structure(record1)
    meta_record2 = meta_structure(record2)
    for k1, v1 in meta_record1.items():
        for k2, v2 in meta_record2.items():
            if k1 == k2:
                mapping[v1] = v2
    return mapping


def extract_nestedvalue(table, keys):
    "Extract values from table given a list of keys and subkeys."
    if len(keys) == 1:
        return table[keys[0]]
    else:
        key = keys[0]
        keys.pop(0)
        subdict = table.get(key)
        return extract_nestedvalue(subdict, keys)


def nest_value(value, table, keys):
    "Nest a value in table viven a list of keys."
    if len(keys) == 1:
        valuetype = table[keys[0]]
        if valuetype == int:
            try:
                table[keys[0]] = int(value)
            except ValueError as e:
                print(e)
                return table
        else:
            table[keys[0]] = value

    else:
        key = keys[0]
        keys.pop(0)
        subdict = table.get(key)
        table[key] = nest_value(subdict, keys, value)
    return table


def convert(record, struc, mapping, separator=","):
    """With a record, a given < struct > as returned by meta_structure(record),
    a mapping as returned by create_mapping(record1, record2), transform record
    structure to < struc > .
    Example: The same item in pica(pica1) and marc(marc1),
    and a second item in pica(pica2).
    marc2 = convert(pica2, extract_structure(marc1), create_mapping(pica1, marc1))"""
    for k, v in mapping.items():
        struc = nest_value(
            extract_nestedvalue(record, k.split(separator)
                                ), struc, v.split(separator)
        )
    return struc
