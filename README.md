# Scripts

Scripts for internal use

## Winibwlist2csv.py

Take a list as produced by winibw as arg 1, reformat it (UTF-8 with BOM to usual utf-8, excel dialect of csv instead of tab separated value), and write the result to the new file given as arg 2.

Example:

`./winibwlist2csv.py ~/Desktop/liste_2023_12_13_18_43_19.txt ~/Desktop/list_2023_12_13.csv`

TIP: I find it more convenient and more efficient to extract a list from hebis via WinIBW 3.7 (K10+) than via WinIBW2002 (hebis) itself.

## csv_extract.py

Reads a csv file and prints one field to stdout (as an array). Can be run in a subshell and passed as argument to other scripts, or stored in a variable.

Example in powershell:
```
$myppnlist=python.exe csv_extract.py --input mylist.csv --column 4 --skip-header
python.exe ppnlist2json.py $myppnlist
```

```
Selects a field out of a csv file and returns an array.

options:
  -h, --help            show this help message and exit
  --input INPUT, -i INPUT
                        The input file.
  --column COLUMN, -c COLUMN
                        The column with ppn. Default=0
  --skip-header, --no-skip-header
                        Skip header (first row).
```

## ppnlist2xml.py && ppnlist2json.py

Accepts ppn as arguments, contacts hebis SRU/SRW server and write a well-formatted xml-Documents based on marcxml records (xml, UTF-8), or a json document. The output file name is given with the flags `-o|--output <name>`, file extension is added. NOTE: The output honors marc structure and as such pretty complex.

Example:
`./ppnlist2xml.py -o mysample 454535074 361987889 454628439 454632940 454627041 454627041`

More advanced (and more interesting) in UNIX-Shell:
`./ppnlist2xml.py -o mysample $(awk -F "," 'FNR > 1 {print $1}' ~/Desktop/list_2023_12_13.csv)`


## helpers.py

A bunch of helpers function:

*Timer* 

    A timer decorator function

*with_output*

    A decorator function: print the index of the element being processed and the length of the array to standard output.

*fetch_xml_hebis*

    A helper function. Takes a ppn as argument. Implements retries and timeout.
    Returns a xml-string encoded in utf-8.

*ppnlist2pandas*

    A helper functions that accepts an array as argument, fetch and parse xml, a return a python-native pandas.Dataframe.
    It also flattens the data: instead of nested values like in marc, each value appears at the top level of a record with an aggregated index key (_code_subcode_). Unlike most native WinIBW lists, that aggregate the values, each subcode has its own datafield for clarity. 

## ppnlist_simple.py

This script uses ppnlist2pandas.py internally and writes to a file with a specific format. The data are flattened, as stated above, and as such more readable (and usable in openrefine, excel, etc.). It doesn't honor marc/pica data structure.

```
usage: simple_formatter.py [-h] [--format {xml,json,csv}] [--output OUTPUT]
                           ppn_array [ppn_array ...]


Processes a list of ppn and returns a file.

positional arguments:
  ppn_array             a space separated list of ppn.

options:
  -h, --help            show this help message and exit
  --format {xml,json,csv}, -f {xml,json,csv}
                        The output format.
  --output OUTPUT, -o OUTPUT
                        The output file. NB. The file extension is generated automatically.
```
